import { type JwtClaims } from "./types"

const decodeRoles = (claims: JwtClaims): string[] => {
    return claims.roles.map((role) => {return role.slice(5)});
}

export default decodeRoles