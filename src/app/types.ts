export interface JwtClaims {
    sub: string;
    exp: number;
    iat: number;
    iss: string;
    roles: string[];
}