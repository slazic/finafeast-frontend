import React from 'react';
import { Navigate, Outlet, useLocation } from 'react-router-dom';
import { useAppSelector } from '../app/hooks';

interface ProtectedRouteProps {
  allowedRoles: string[];
}

const ProtectedRoute = ({ allowedRoles }: ProtectedRouteProps): JSX.Element => {
  const location = useLocation();
  const userRoles = useAppSelector(state => {
    return state.authentication.userRoles;
  });

  return userRoles.length > 0 ? (
    allowedRoles.some(allowedRole => userRoles.includes(allowedRole)) ? (
      <Outlet />
    ) : (
      <Navigate to={'/unauthorized'} state={{ from: location }} replace />
    )
  ) : (
    <Navigate to={'/login'} state={{ from: location }} replace />
  );
};

export default ProtectedRoute;
