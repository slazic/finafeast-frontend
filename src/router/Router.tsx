import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from '../pages/HomePage';
import LoginPage from '../pages/LoginPage';
import RegistrationPage from '../pages/RegistrationPage';
import Layout from '../layout/Layout';
import RestaurantsPage from '../pages/RestaurantsPage';
import MenuPage from '../pages/MenuPage';
import OrdersPage from '../pages/OrdersPage';
import ProtectedRoute from './ProtectedRoute';
import CreateOrderPage from '../pages/CreateOrderPage';
import AvailableOrdersForDeliveryPage from '../pages/AvailableOrdersForDeliveryPage';

const Router = (): JSX.Element => {
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <Layout>
              <HomePage />
            </Layout>
          }
        />
        <Route
          path="/restaurants"
          element={
            <Layout>
              <RestaurantsPage />
            </Layout>
          }
        />
        <Route
          path="/menu"
          element={
            <Layout>
              <MenuPage />
            </Layout>
          }
        />

        <Route
          element={
            <Layout>
              <ProtectedRoute
                allowedRoles={['USER', 'MANAGER', 'ADMIN', 'DELIVERER']}
              />
            </Layout>
          }
        >
          <Route path="/orders" element={<OrdersPage />} />
          <Route path="/createOrder" element={<CreateOrderPage />} />
          <Route
            path="/availableOrders"
            element={<AvailableOrdersForDeliveryPage />}
          />
        </Route>
        <Route path="/login" element={<LoginPage />} />
        <Route path="/registration" element={<RegistrationPage />} />
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
