import { combineReducers, configureStore } from "@reduxjs/toolkit";
import authenticationSlice from "./feature/authenticationSlice";
import { persistReducer } from 'redux-persist'

import storage from 'redux-persist/lib/storage';

const reducers = combineReducers({
    authentication: authenticationSlice
})

const persistConfig = {
    key: 'root',
    version: 1,
    storage
}

const persistedReducer = persistReducer(persistConfig, reducers)

export const store = configureStore({
    reducer: persistedReducer
});

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

