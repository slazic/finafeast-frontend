import { type PayloadAction, createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import { apiLogin } from '../../api/authentication'
import { type ApiLoginFormProps } from '../../components/forms/LoginForm'
import { jwtDecode } from 'jwt-decode'
import { type JwtClaims } from '../../app/types'
import decodeRoles from '../../app/decodeRoles'

export interface AuthenticationState {
    jwt: string,
    userRoles: string[]
}

const initialState: AuthenticationState = {
    jwt: '',
    userRoles: []
}

export const authenticateUser = createAsyncThunk("authentication/save", async (values: ApiLoginFormProps, thunkApi): Promise<string> => {
    const response = await apiLogin(values);
    return response.data
})

export const authenticationSlice = createSlice({
    name: 'authentication',
    initialState,
    reducers: {
        login: (state, action:PayloadAction<{jwt: string}>) => {
            if(action.payload.jwt === '') {
                state.userRoles = []
                state.jwt = ''
            } else {
                const claims: JwtClaims = jwtDecode(action.payload.jwt)
                state.userRoles = decodeRoles(claims);
                state.jwt = action.payload.jwt
            }
        }
    },
    extraReducers: (builder) => {
        builder.addCase(authenticateUser.fulfilled, (state, action) => {
            state.jwt = action.payload
        })
    }
})

export const { login } = authenticationSlice.actions;

export default authenticationSlice.reducer;