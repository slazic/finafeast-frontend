import axios from "axios";
import { type ApiLoginFormProps } from "../components/forms/LoginForm";
import { type ApiRegistrationFormProps } from "../components/forms/RegistrationForm";

export interface ApiAuthResponse {
    data: string;
    status: number;
    statusText: string;
}

export interface ApiLogoutResponse {
    status: number
}

const apiLogin = async (values: ApiLoginFormProps): Promise<ApiAuthResponse> => {
    return await axios.post('http://localhost:8080/users/login', values);
}

const apiLogout = async (): Promise<ApiLogoutResponse> => {
    return await axios.post('http://localhost:8080/logout');
}

const apiRegister = async (values: ApiRegistrationFormProps): Promise<ApiAuthResponse> => {
    return await axios.post('http://localhost:8080/users/registration', values);
}

export { apiLogin, apiLogout, apiRegister }