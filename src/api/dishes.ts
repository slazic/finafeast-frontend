import axios from "axios";
import { type Dish, type DishThunk } from "../components/menu/MenuTable";

export interface ApiDishesResponse {
    data: [],
    status: number,
    statusText: string,
}

const fetchAllDishes = async (): Promise<ApiDishesResponse> => {
    return await axios.get("http://localhost:8080/dishes/all");
}

const createDish = async (dish: DishThunk, jwt: string): Promise<ApiDishesResponse> => {
    return await axios.post("http://localhost:8080/dishes/create", dish, {headers: {Authorization: `Bearer ${jwt}`}})
}

const deleteDish = async (dishId: number | undefined, jwt:string): Promise<ApiDishesResponse> => {
    return await axios.delete("http://localhost:8080/dishes/delete", { params: {id: dishId}, headers: {Authorization: `Bearer ${jwt}`}});
}

const updateDish = async (dish: Dish, jwt: string): Promise<ApiDishesResponse> => {
    return await axios.put("http://localhost:8080/dishes/update", dish, {headers: {Authorization: `Bearer ${jwt}`}})
}

const fetchAllDishesByOrderId = async (id: number | undefined): Promise<ApiDishesResponse> => {
    return await axios.get("http://localhost:8080/dishes/allBy", {params : {orderId: id}})
}

export { fetchAllDishes, createDish, deleteDish, updateDish, fetchAllDishesByOrderId }