import axios from "axios";
import { type RestaurantThunk } from "../components/restaurants/RestaurantsTable";

export interface ApiRestaurantsResponse {
    data: []
    status: number;
    statusText: string;
}

export interface ApiUpdateRestaurantRequest {
    id: number;
    name: string;
    phone: string;
    address: string;
    managerId: number;
  }

const fetchAllRestaurants = async (): Promise<ApiRestaurantsResponse> => {
    return await axios.get("http://localhost:8080/restaurants/all");
}

const createRestaurant = async (values: RestaurantThunk, jwt: string): Promise<ApiRestaurantsResponse> => {
    return await axios.post("http://localhost:8080/restaurants/create", values, {headers: {Authorization: `Bearer ${jwt}`}});
}

const updateRestaurant = async (values: ApiUpdateRestaurantRequest, jwt: string): Promise<ApiRestaurantsResponse> => {
    return await axios.put("http://localhost:8080/restaurants/update", values, {headers: {Authorization: `Bearer ${jwt}`}})
}

const deleteRestaurant = async (value: number | undefined, jwt: string): Promise<ApiRestaurantsResponse> => {
    return await axios.delete("http://localhost:8080/restaurants/delete", {params: {id: value}, headers: {Authorization: `Bearer ${jwt}`}})
}

export { fetchAllRestaurants, updateRestaurant, deleteRestaurant, createRestaurant }