import axios from "axios";
import { type CreateOrderFormProps } from "../components/forms/CreateOrderForm";

export interface ApiOrdersResponse {
    data: [];
    status: number;
    statusText: string;
}

const createOrder = async (values: CreateOrderFormProps, jwt: string): Promise<ApiOrdersResponse> => {
    return await axios.post("http://localhost:8080/orders/create", values, {headers: {Authorization: `Bearer ${jwt}`}})
}

const fetchAllOrders = async(jwt: string):Promise<ApiOrdersResponse> => {
    return await axios.get("http://localhost:8080/orders/all", {headers: {Authorization: `Bearer ${jwt}`}})
}

const fetchAllUserOrders = async(jwt: string):Promise<ApiOrdersResponse> => {
    return await axios.get("http://localhost:8080/orders/user/all", {headers: {Authorization: `Bearer ${jwt}`}})
}

const fetchAllRestaurantOrders = async (jwt: string): Promise<ApiOrdersResponse> => {
    return await axios.get("http://localhost:8080/orders/restaurant/all", {headers: {Authorization: `Bearer ${jwt}`}})
}

const fetchAllDelivererAvailableOrders = async (jwt:string): Promise<ApiOrdersResponse> => {
    return await axios.get("http://localhost:8080/orders/deliverer/available", {headers: {Authorization: `Bearer ${jwt}`}})
}

const fetchAllDelivererOrders = async (jwt: string): Promise<ApiOrdersResponse> => {
    return await axios.get("http://localhost:8080/orders/deliverer/all", {headers: {Authorization: `Bearer ${jwt}`}})
}

const deleteOrder = async (orderId: number | undefined, jwt: string): Promise<ApiOrdersResponse> => {
    return await axios.delete("http://localhost:8080/orders/delete", {params: {id: orderId}, headers: {Authorization: `Bearer ${jwt}`}})
}

const updateOrderStatus = async (orderId: number | undefined, jwt:string): Promise<ApiOrdersResponse> => {
    return await axios.put("http://localhost:8080/orders/updateStatus", null, {params: {orderId}, headers: {Authorization: `Bearer ${jwt}`}})
}

const setOrderDeliverer = async (orderId: number | undefined, jwt: string): Promise<ApiOrdersResponse> => {
    return await axios.put("http://localhost:8080/orders/setDeliverer", null, {params: {orderId}, headers: {Authorization: `Bearer ${jwt}`}})

}

export { 
    fetchAllOrders, 
    fetchAllUserOrders, 
    fetchAllRestaurantOrders, 
    fetchAllDelivererAvailableOrders, 
    fetchAllDelivererOrders, 
    createOrder, 
    deleteOrder,
    updateOrderStatus,
    setOrderDeliverer
}