import axios from "axios";

export interface ApiUsersResponse {
    data: [],
    status: number,
    statusText: string,
}

const fetchAllUsersByRole = async (roleName: string, jwt: string): Promise<ApiUsersResponse> => {
    return await axios.get("http://localhost:8080/users/allByRole", {params: {name: roleName}, headers: {Authorization: `Bearer ${jwt}`}});
}

export { fetchAllUsersByRole }