import React from 'react';
import LoginForm from '../components/forms/LoginForm';
import { Card } from 'antd';

const LoginPage = (): JSX.Element => {
  return (
    <Card title="Sign in" bordered={false} className="container mt-6">
      <LoginForm />
    </Card>
  );
};

export default LoginPage;
