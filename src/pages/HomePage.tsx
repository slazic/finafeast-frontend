import { message } from 'antd';
import React, { useEffect, useRef } from 'react';
import { useLocation } from 'react-router-dom';

const HomePage = (): JSX.Element => {
  const [messageApi, contextHolder] = message.useMessage();
  const { state } = useLocation();
  const effectRan = useRef(false);

  useEffect(() => {
    if (state !== null && effectRan.current) {
      void messageApi.open({
        type: 'success',
        content: state.authRedirectMessage,
      });
    }
    return () => {
      effectRan.current = true;
    };
  }, [state]);

  return (
    <>
      {contextHolder}
      <p className="font-lg">This is a homepage on FinaFeast.</p>
    </>
  );
};

export default HomePage;
