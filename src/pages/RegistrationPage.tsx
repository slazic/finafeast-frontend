import { Card } from 'antd';
import React from 'react';
import RegistrationForm from '../components/forms/RegistrationForm';

const RegistrationPage = (): JSX.Element => {
  return (
    <Card title="Register" bordered={false} className="container mt-6">
      <RegistrationForm />
    </Card>
  );
};

export default RegistrationPage;
