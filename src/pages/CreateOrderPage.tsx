import React from 'react';
import CreateOrderForm from '../components/forms/CreateOrderForm';

const CreateOrderPage = (): JSX.Element => {
  return <CreateOrderForm />;
};

export default CreateOrderPage;
