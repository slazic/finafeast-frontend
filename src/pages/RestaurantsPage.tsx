import React from 'react';
import RestaurantsTable from '../components/restaurants/RestaurantsTable';

const RestaurantsPage = (): JSX.Element => {
  return (
    <>
      <RestaurantsTable />
    </>
  );
};

export default RestaurantsPage;
