import React from 'react';
import OrdersList from '../components/orders/OrdersList';

const OrdersPage = (): JSX.Element => {
  return <OrdersList />;
};

export default OrdersPage;
