import React from 'react';
import MenuTable from '../components/menu/MenuTable';

const MenuPage = (): JSX.Element => {
  return <MenuTable />;
};

export default MenuPage;
