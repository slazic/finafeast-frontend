import React from 'react';
import AvailableOrdersForDeliveryList from '../components/orders/AvailableOrdersForDeliveryList';

const AvailableOrdersForDeliveryPage = (): JSX.Element => {
  return <AvailableOrdersForDeliveryList />;
};

export default AvailableOrdersForDeliveryPage;
