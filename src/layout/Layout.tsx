import React from 'react';
import { Layout as AntdLayout } from 'antd';
import MainMenu from '../components/MainMenu';
import HeaderMenu from '../components/HeaderMenu';

const { Content, Sider } = AntdLayout;

interface IProps {
  children: JSX.Element;
}

const Layout = ({ children }: IProps): JSX.Element => {
  return (
    <AntdLayout style={{ minHeight: '100vh' }}>
      <Sider>
        <MainMenu />
      </Sider>
      <AntdLayout>
        <HeaderMenu />
        <Content>{children}</Content>
      </AntdLayout>
    </AntdLayout>
  );
};

export default Layout;
