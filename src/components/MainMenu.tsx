import {
  GiftFilled,
  HomeFilled,
  PicLeftOutlined,
  ShopFilled,
} from '@ant-design/icons';
import { Menu } from 'antd';
import React from 'react';
import { NavLink } from 'react-router-dom';
import { useAppSelector } from '../app/hooks';

const MainMenu = (): JSX.Element => {
  const jwt = useAppSelector(state => {
    return state.authentication.jwt;
  });
  const userRoles = useAppSelector(state => {
    return state.authentication.userRoles;
  });

  return (
    <>
      <Menu mode="inline" theme="dark">
        <Menu.Item key="home" icon={<HomeFilled />}>
          <NavLink to="/">HOME</NavLink>
        </Menu.Item>
        <Menu.Item key="restaurants" icon={<ShopFilled />}>
          <NavLink to="/restaurants">RESTAURANTS</NavLink>
        </Menu.Item>
        <Menu.Item key="menu" icon={<PicLeftOutlined />}>
          <NavLink to="/menu">MENU</NavLink>
        </Menu.Item>
        {jwt !== '' ? (
          <Menu.SubMenu
            key="ordersSubmenu"
            icon={<GiftFilled />}
            title="ORDERS"
          >
            {userRoles.some(userRole => ['USER', 'ADMIN'].includes(userRole)) &&
            !userRoles.some(userRole =>
              ['DELIVERER', 'MANAGER'].includes(userRole)
            ) ? (
              <Menu.Item key="createOrder">
                <NavLink to="/createOrder">CREATE ORDER</NavLink>
              </Menu.Item>
            ) : (
              <></>
            )}
            <Menu.Item key="allOrders">
              <NavLink to="/orders">ALL ORDERS</NavLink>
            </Menu.Item>
            {userRoles.some(userRole => ['DELIVERER'].includes(userRole)) ? (
              <Menu.Item key="availableOrders">
                <NavLink to="/availableOrders">AVAILABLE ORDERS</NavLink>
              </Menu.Item>
            ) : (
              <></>
            )}
          </Menu.SubMenu>
        ) : (
          <></>
        )}
      </Menu>
    </>
  );
};

export default MainMenu;
