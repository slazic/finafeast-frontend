import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../app/hooks';
import { login } from '../store/feature/authenticationSlice';
import { apiLogout } from '../api/authentication';
import { Button } from 'antd';

const MainMenu = (): JSX.Element => {
  const jwt = useAppSelector(state => {
    return state.authentication.jwt;
  });

  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const onLogout = async (): Promise<void> => {
    await apiLogout()
      .then(response => {
        dispatch(login({ jwt: '' }));
        navigate('/');
      })
      .catch(error => {
        console.log(error);
      });
  };
  return (
    <div>
      <ul
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          listStyle: 'none',
          float: 'right',
          paddingInline: 20,
        }}
      >
        {jwt !== '' ? (
          <li style={{ padding: 10, textDecoration: 'none' }}>
            <Button
              type="link"
              onClick={() => {
                void onLogout();
                navigate('/', {
                  state: { authRedirectMessage: 'Succesfull logout.' },
                });
              }}
            >
              LOGOUT
            </Button>
          </li>
        ) : (
          <>
            <li style={{ padding: 10, textDecoration: 'none' }}>
              <Link to="/login">LOGIN</Link>
            </li>
            <li style={{ padding: 10, textDecoration: 'none' }}>
              <Link to="/registration">REGISTRATION</Link>
            </li>
          </>
        )}
      </ul>
    </div>
  );
};

export default MainMenu;
