import { Button, Modal, Table, message } from 'antd';
import React, { useEffect, useState } from 'react';
import { type Order } from './OrdersList';
import { type Dish } from '../menu/MenuTable';
import { fetchAllDishesByOrderId } from '../../api/dishes';
import { type ColumnsType } from 'antd/es/table';
import {
  type ApiOrdersResponse,
  deleteOrder,
  updateOrderStatus,
  setOrderDeliverer,
} from '../../api/orders';
import { useAppSelector } from '../../app/hooks';

interface OrdersActionsModalProps {
  order: Order | undefined;
  isModalOpen: boolean;
  handleCancel: () => void;
  setResponseMethod: (value: ApiOrdersResponse) => void;
}

const OrdersActionsModal = ({
  order,
  isModalOpen,
  handleCancel,
  setResponseMethod,
}: OrdersActionsModalProps): JSX.Element => {
  const [dishes, setDishes] = useState<Dish[]>();
  const [loading, setLoading] = useState<boolean>(true);
  const jwt = useAppSelector(state => state.authentication.jwt);
  const userRoles = useAppSelector(state => state.authentication.userRoles);
  const [messageApi, contextHolder] = message.useMessage();

  const handleDelete = async (value: number | undefined): Promise<void> => {
    await deleteOrder(value, jwt)
      .then(response => {
        setResponseMethod(response);
        void messageApi.open({
          type: 'success',
          content: response.data,
        });
      })
      .catch(error => {
        console.log(error.response);
        setResponseMethod(error.response);
      });
  };

  const handleUpdateStatus = async (
    value: number | undefined
  ): Promise<void> => {
    await updateOrderStatus(value, jwt)
      .then(response => {
        setResponseMethod(response);
        void messageApi.open({
          type: 'success',
          content: response.data,
        });
      })
      .catch(error => {
        console.log(error.response);
        setResponseMethod(error.response);
      });
  };

  const handleSetDeliverer = async (
    value: number | undefined
  ): Promise<void> => {
    await setOrderDeliverer(value, jwt)
      .then(response => {
        setResponseMethod(response);
        void messageApi.open({
          type: 'success',
          content: response.data,
        });
      })
      .catch(error => {
        console.log(error.response);
        setResponseMethod(error.response);
      });
  };

  useEffect(() => {
    const loadAllDishes = async (): Promise<void> => {
      await fetchAllDishesByOrderId(order?.id)
        .then(response => {
          setDishes(response.data);
        })
        .catch(error => {
          console.log(error.response);
          if (
            error.response === undefined ||
            (error.response.data === 'Dishes not found.' &&
              error.response.status === 400)
          ) {
            setDishes([]);
          }
        })
        .finally(() => {
          setLoading(false);
        });
    };

    void loadAllDishes();
  }, [order?.id]);

  const columns: ColumnsType<Dish> = [
    {
      title: 'Name',
      dataIndex: 'dishName',
      key: 'name',
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
    },
  ];

  const footerButtons = (): JSX.Element[] => {
    switch (true) {
      case userRoles.includes('ADMIN') &&
        order?.deliverer === null &&
        order.status !== 'DELIVERED':
        return [
          <Button
            key="cancelButton"
            onClick={() => {
              handleCancel();
            }}
          >
            Cancel
          </Button>,

          <Button
            key="chooseButton"
            onClick={() => {
              void handleSetDeliverer(order?.id);
              handleCancel();
            }}
          >
            Deliver this order
          </Button>,

          <Button
            key="updateButton"
            onClick={() => {
              void handleUpdateStatus(order?.id);
              handleCancel();
            }}
          >
            Update status
          </Button>,

          <Button
            key="deleteButton"
            type="primary"
            danger
            onClick={() => {
              void handleDelete(order?.id);
              handleCancel();
            }}
          >
            Delete
          </Button>,
        ];
      case userRoles.includes('ADMIN') &&
        order?.deliverer !== null &&
        order?.status !== 'DELIVERED':
        return [
          <Button
            key="cancelButton"
            onClick={() => {
              handleCancel();
            }}
          >
            Cancel
          </Button>,

          <Button
            key="updateButton"
            onClick={() => {
              void handleUpdateStatus(order?.id);
              handleCancel();
            }}
          >
            Update status
          </Button>,

          <Button
            key="deleteButton"
            type="primary"
            danger
            onClick={() => {
              void handleDelete(order?.id);
              handleCancel();
            }}
          >
            Delete
          </Button>,
        ];
      case userRoles.includes('ADMIN') &&
        order?.deliverer !== null &&
        order?.status === 'DELIVERED':
        return [
          <Button
            key="cancelButton"
            onClick={() => {
              handleCancel();
            }}
          >
            Cancel
          </Button>,
          <Button
            key="deleteButton"
            type="primary"
            danger
            onClick={() => {
              void handleDelete(order?.id);
              handleCancel();
            }}
          >
            Delete
          </Button>,
        ];
      case userRoles.includes('ADMIN') &&
        order?.deliverer === null &&
        order?.status === 'DELIVERED':
        return [
          <Button
            key="cancelButton"
            onClick={() => {
              handleCancel();
            }}
          >
            Cancel
          </Button>,
          <Button
            key="chooseButton"
            onClick={() => {
              void handleSetDeliverer(order?.id);
              handleCancel();
            }}
          >
            Deliver this order
          </Button>,
          <Button
            key="deleteButton"
            type="primary"
            danger
            onClick={() => {
              void handleDelete(order?.id);
              handleCancel();
            }}
          >
            Delete
          </Button>,
        ];
      case userRoles.includes('MANAGER') &&
        (order?.status === 'CREATED' || order?.status === 'PREPARING'):
        return [
          <Button
            key="cancelButton"
            onClick={() => {
              handleCancel();
            }}
          >
            Cancel
          </Button>,
          <Button
            key="updateButton"
            onClick={() => {
              void handleUpdateStatus(order?.id);
              handleCancel();
            }}
          >
            Update status
          </Button>,
        ];
      case userRoles.includes('DELIVERER') &&
        order?.deliverer !== null &&
        (order?.status === 'PREPARED' || order?.status === 'DELIVERING'):
        return [
          <Button
            key="cancelButton"
            onClick={() => {
              handleCancel();
            }}
          >
            Cancel
          </Button>,
          <Button
            key="updateButton"
            onClick={() => {
              void handleUpdateStatus(order?.id);
              handleCancel();
            }}
          >
            Update status
          </Button>,
        ];
      case userRoles.includes('DELIVERER') && order?.deliverer === null:
        return [
          <Button
            key="cancelButton"
            onClick={() => {
              handleCancel();
            }}
          >
            Cancel
          </Button>,
          <Button
            key="chooseButton"
            onClick={() => {
              void handleSetDeliverer(order?.id);
              handleCancel();
            }}
          >
            Deliver this order
          </Button>,
        ];
      default:
        return [
          <Button
            key="cancelButton"
            onClick={() => {
              handleCancel();
            }}
          >
            Cancel
          </Button>,
        ];
    }
  };

  return (
    <>
      {contextHolder}
      <Modal
        title={'This is the order -> id: ' + order?.id + '.'}
        open={isModalOpen}
        width={1000}
        footer={footerButtons}
      >
        <Table
          columns={columns}
          dataSource={dishes}
          loading={loading}
          className="mt-6 mx"
          pagination={false}
        />
        <p className="font-bold text-right font-lg mt-6 mb-6">
          Total price: €
          {dishes?.reduce((totalPrice, dish) => totalPrice + dish.price, 0)}
        </p>
      </Modal>
    </>
  );
};

export default OrdersActionsModal;
