import { Badge, Card, Flex } from 'antd';
import React, { useEffect, useState } from 'react';
import { useAppSelector } from '../../app/hooks';
import {
  type ApiOrdersResponse,
  fetchAllDelivererAvailableOrders,
} from '../../api/orders';
import {
  CalendarFilled,
  HomeFilled,
  LoadingOutlined,
  PhoneFilled,
  ShopFilled,
  UserOutlined,
} from '@ant-design/icons';
import OrdersActionsModal from './OrdersActionsModal';

interface OrderRestaurant {
  id: number;
  name: string;
  phone: string;
  address: string;
  manager: OrderUser;
}

interface OrderUser {
  id: number;
  firstName: string;
  lastName: string;
  phone: string;
  address: string;
  email: string;
}

export interface Order {
  id: number;
  dateFrom: string;
  dateTo?: string;
  user: OrderUser;
  deliverer: OrderUser;
  restaurant: OrderRestaurant;
  status: string;
}

const AvailableOrdersForDeliveryList = (): JSX.Element => {
  const jwt = useAppSelector(state => {
    return state.authentication.jwt;
  });

  const [orders, setOrders] = useState<Order[]>();
  const [loading, setLoading] = useState<boolean>(true);
  const [showModal, setShowModal] = useState<boolean>(false);
  const [selectedOrder, setSelectedOrder] = useState<Order>();
  const [response, setResponse] = useState<ApiOrdersResponse>();

  const handleOpenModal = (selectedOrderId: number): void => {
    const order = orders?.filter(order => order.id === selectedOrderId)[0];
    setSelectedOrder(order);
    setShowModal(true);
  };

  useEffect(() => {
    const loadAllOrders = async (): Promise<void> => {
      await fetchAllDelivererAvailableOrders(jwt)
        .then(response => {
          setOrders(response.data);
        })
        .catch(error => {
          console.log(error.response);
        })
        .finally(() => {
          setLoading(false);
        });
    };
    void loadAllOrders();
  }, [response]);

  return (
    <>
      {loading ? <LoadingOutlined className="icon-loading" /> : <></>}
      <Flex wrap="wrap" gap="large" justify="center" className="mt-6 mb-6">
        {orders?.map(order => {
          let badge: JSX.Element = <></>;
          let ribbonColor: string = '';

          switch (order.status) {
            case 'CREATED':
              badge = (
                <Badge
                  status="default"
                  text={order.status + ' -> ' + order.id}
                />
              );
              ribbonColor = 'purple';
              break;
            case 'PREPARING':
              badge = (
                <Badge
                  status="processing"
                  text={order.status + ' -> ' + order.id}
                />
              );
              break;
            case 'PREPARED':
              badge = (
                <Badge
                  status="warning"
                  text={order.status + ' -> ' + order.id}
                />
              );
              ribbonColor = 'orange';
              break;
            case 'DELIVERING':
              badge = (
                <Badge status="error" text={order.status + ' -> ' + order.id} />
              );
              ribbonColor = 'volcano';
              break;
            case 'DELIVERED':
              badge = (
                <Badge
                  status="success"
                  text={order.status + ' -> ' + order.id}
                />
              );
              ribbonColor = 'green';
              break;
            default:
              badge = <></>;
              ribbonColor = '';
          }

          return (
            <Badge.Ribbon
              text={order.status}
              color={ribbonColor}
              key={order.id}
            >
              <Card
                title={badge}
                bordered={false}
                style={{ width: 400 }}
                onClick={() => {
                  handleOpenModal(order.id);
                }}
                hoverable
              >
                <Flex gap={'middle'}>
                  <div style={{ textAlign: 'left' }}>
                    <p>
                      User <UserOutlined />:
                    </p>
                    <p>
                      Address <HomeFilled />:
                    </p>
                    <p>
                      Phone <PhoneFilled />:
                    </p>
                    <p>
                      Restaurant <ShopFilled />:
                    </p>
                    {order.deliverer !== null ? (
                      <p>
                        Deliverer <UserOutlined />:
                      </p>
                    ) : (
                      <p>
                        Deliverer <UserOutlined />:
                      </p>
                    )}
                    <p>
                      Date from <CalendarFilled />:
                    </p>
                    {order.dateTo !== null ? (
                      <p>
                        Date to <CalendarFilled />:
                      </p>
                    ) : (
                      <p>
                        Date to <CalendarFilled />:
                      </p>
                    )}
                  </div>
                  <div style={{ textAlign: 'left' }}>
                    <p>{order.user.firstName}</p>
                    <p>{order.user.address}</p>
                    <p>{order.user.phone}</p>
                    <p>
                      {order.restaurant.name} {order.restaurant.address}
                    </p>
                    {order.deliverer !== null ? (
                      <p>{order.deliverer?.firstName}</p>
                    ) : (
                      <p>Not selected.</p>
                    )}
                    <p>{order.dateFrom}</p>
                    {order.dateTo !== null ? (
                      <p>{order.dateTo}</p>
                    ) : (
                      <p>Not delivered yet.</p>
                    )}
                  </div>
                </Flex>
              </Card>
            </Badge.Ribbon>
          );
        })}
      </Flex>
      <OrdersActionsModal
        isModalOpen={showModal}
        order={selectedOrder}
        handleCancel={() => {
          setShowModal(false);
        }}
        setResponseMethod={setResponse}
      />
    </>
  );
};

export default AvailableOrdersForDeliveryList;
