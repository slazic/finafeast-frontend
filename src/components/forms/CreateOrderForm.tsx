import { Button, Form, Select, message } from 'antd';
import React, { useEffect, useState } from 'react';
import { type Restaurant } from '../restaurants/RestaurantsTable';
import { type Dish } from '../menu/MenuTable';
import { fetchAllDishes } from '../../api/dishes';
import { fetchAllRestaurants } from '../../api/restaurants';
import { useForm } from 'antd/es/form/Form';
import { createOrder } from '../../api/orders';
import { useAppSelector } from '../../app/hooks';

export interface CreateOrderFormProps {
  restaurantId: number;
  selectedDishesIds: number[];
}

const CreateOrderForm = (): JSX.Element => {
  const [restaurants, setRestaurants] = useState<Restaurant[]>();
  const [dishes, setDishes] = useState<Dish[]>();
  const [loading, setLoading] = useState<boolean>(true);
  const [form] = useForm();
  const jwt = useAppSelector(state => {
    return state.authentication.jwt;
  });
  const [messageApi, contextHolder] = message.useMessage();

  const onFinish = async (values: CreateOrderFormProps): Promise<void> => {
    await createOrder(values, jwt)
      .then(response => {
        void messageApi.open({
          type: 'success',
          content: response.data,
        });
      })
      .catch(error => {
        console.log(error.response);
        void messageApi.open({
          type: 'error',
          content: error.response.data,
        });
      })
      .finally(() => {
        form.resetFields();
      });
  };

  const onFinishFailed = (errorInfo: any): void => {
    console.log('Failed:', errorInfo);
  };

  useEffect(() => {
    const loadAllDishes = async (): Promise<void> => {
      await fetchAllDishes()
        .then(response => {
          setDishes(response.data);
        })
        .catch(error => {
          console.log(error.response);
          if (
            error.response === undefined ||
            (error.response.data === 'Dishes not found.' &&
              error.response.status === 404)
          ) {
            setDishes([]);
          }
        })
        .finally(() => {
          setLoading(false);
        });
    };

    const loadAllRestaurants = async (): Promise<void> => {
      await fetchAllRestaurants()
        .then(response => {
          setRestaurants(response.data);
        })
        .catch(error => {
          console.log(error.response);
          if (
            error.response === undefined ||
            (error.response.status === 404 &&
              error.response.data === 'Restaurants not found.')
          ) {
            setRestaurants([]);
          }
        })
        .finally(() => {
          setLoading(false);
        });
    };

    void loadAllRestaurants();
    void loadAllDishes();
  }, []);

  return (
    <>
      {contextHolder}
      <Form
        name="loginForm"
        className="mt-6 mx"
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        form={form}
      >
        <Form.Item
          name="restaurantId"
          rules={[
            {
              required: true,
              message: 'Restaurant is required.',
            },
          ]}
        >
          <Select
            allowClear
            placeholder="Select a restaurant."
            optionFilterProp="children"
            options={restaurants?.map(restaurant => {
              return {
                value: restaurant.id,
                label: restaurant.name + ' ' + restaurant.address,
              };
            })}
            loading={loading}
          />
        </Form.Item>
        <Form.Item
          name="selectedDishesIds"
          rules={[
            {
              required: true,
              message: 'Select at least one dish.',
            },
          ]}
        >
          <Select
            mode="multiple"
            allowClear
            style={{ width: '100%' }}
            placeholder="Please select dishes."
            options={dishes?.map(dish => {
              return {
                value: dish.id,
                label: dish.dishName,
              };
            })}
            loading={loading}
          />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" className="">
            Place order
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default CreateOrderForm;
