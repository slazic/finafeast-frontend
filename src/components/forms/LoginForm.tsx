import React from 'react';

import { LockOutlined, MailOutlined } from '@ant-design/icons';
import { Button, Form, Input, message } from 'antd';
import { Link /* useNavigate */, useNavigate } from 'react-router-dom';
import { apiLogin } from '../../api/authentication';
import { useAppDispatch } from '../../app/hooks';
import { login } from '../../store/feature/authenticationSlice';

export interface ApiLoginFormProps {
  username?: string;
  password?: string;
}

const LoginForm = (): JSX.Element => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const [messageApi, contextHolder] = message.useMessage();

  const onFinish = async (values: ApiLoginFormProps): Promise<void> => {
    await apiLogin(values)
      .then(response => {
        dispatch(login({ jwt: response.data }));
      })
      .then(() => {
        navigate('/', { state: { authRedirectMessage: 'Succesfull login.' } });
      })
      .catch(error => {
        console.log(error.response);
        void messageApi.open({
          type: 'error',
          content: error.response.data,
        });
      });
    // await dispatch(authenticateUser(values));
  };

  const onFinishFailed = (errorInfo: any): void => {
    console.log('Failed:', errorInfo);
  };

  return (
    <>
      {contextHolder}
      <Form
        name="loginForm"
        className=""
        // eslint-disable-next-line @typescript-eslint/no-misused-promises
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          name="email"
          rules={[
            {
              type: 'email',
              message: 'The input is not valid E-mail.',
            },
            {
              required: true,
              message: 'Please input your E-mail.',
            },
          ]}
        >
          <Input
            prefix={<MailOutlined className="icon-opacity" />}
            placeholder="Email"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: 'Password is required.',
            },
          ]}
        >
          <Input.Password
            prefix={<LockOutlined className="icon-opacity" />}
            placeholder="Password"
          />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" className="">
            Log in
          </Button>
        </Form.Item>
        <p>
          Don&apos;t have an account? Register{' '}
          <Link to="/registration">here</Link>.
        </p>
      </Form>
    </>
  );
};

export default LoginForm;
