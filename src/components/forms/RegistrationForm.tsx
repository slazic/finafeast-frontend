import { LockOutlined, MailOutlined } from '@ant-design/icons';
import {
  Cascader,
  type CascaderProps,
  Form,
  Input,
  Select,
  Checkbox,
  Button,
} from 'antd';
import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { apiRegister } from '../../api/authentication';

export interface ApiRegistrationFormProps {
  firstName: string;
  lastName: string;
  phone: string;
  address: string;
  email: string;
  password: string;
}

interface RegistrationFormProps {
  firstName: string;
  lastName: string;
  phonePrefix: string;
  phone: string;
  address: string[];
  email: string;
  password: string;
  confirmPassword: string;
}

interface DataNodeType {
  value: string;
  label: string;
  children?: DataNodeType[];
}

const residences: CascaderProps<DataNodeType>['options'] = [
  {
    value: 'Osijek',
    label: 'Osijek',
    children: [
      {
        value: 'Ul. Lorenza Jagera',
        label: 'Ul. Lorenza Jagera',
        children: [
          {
            value: '1',
            label: '1',
          },
        ],
      },
      {
        value: 'Ul. Ive Tijardovića',
        label: 'Ul. Ive Tijardovića',
        children: [
          {
            value: '4A',
            label: '4A',
          },
        ],
      },
    ],
  },
];

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 5 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 4,
    },
  },
};

const RegistrationForm = (): JSX.Element => {
  const navigate = useNavigate();

  const onFinish = async (values: RegistrationFormProps): Promise<void> => {
    const registrationInformation: ApiRegistrationFormProps = {
      firstName: values.firstName,
      lastName: values.lastName,
      phone: values.phonePrefix + values.phone,
      address:
        values.address[1] + ' ' + values.address[2] + ', ' + values.address[0],
      email: values.email,
      password: values.confirmPassword,
    };

    await apiRegister(registrationInformation)
      .then(response => {
        navigate('/');
      })
      .catch(error => {
        console.log(error);
      });
  };

  const onFinishFailed = (errorInfo: any): void => {
    console.log('Failed:', errorInfo);
  };

  const prefixSelector = (
    <Form.Item name="phonePrefix" noStyle>
      <Select>
        <Select.Option value={'+385'}>+385</Select.Option>
      </Select>
    </Form.Item>
  );

  return (
    <Form
      name="registrationForm"
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      initialValues={{ phonePrefix: '+385' }}
      scrollToFirstError
      {...formItemLayout}
    >
      <Form.Item
        name="firstName"
        label="First name"
        rules={[
          {
            required: true,
            message: 'Please input your first name.',
            whitespace: true,
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="lastName"
        label="Last name"
        rules={[
          {
            required: true,
            message: 'Please input your last name.',
            whitespace: true,
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name="phone"
        label="Phone number"
        rules={[{ required: true, message: 'Please input your phone number.' }]}
      >
        <Input addonBefore={prefixSelector} style={{ width: '100%' }} />
      </Form.Item>
      <Form.Item
        name="address"
        label="Address"
        rules={[
          {
            type: 'array',
            required: true,
            message: 'Please select your address.',
          },
        ]}
      >
        <Cascader options={residences} />
      </Form.Item>
      <Form.Item
        name="email"
        label="E-mail"
        rules={[
          {
            type: 'email',
            message: 'The input is not valid E-mail.',
          },
          {
            required: true,
            message: 'Please input your E-mail.',
          },
        ]}
      >
        <Input prefix={<MailOutlined className="icon-opacity" />} />
      </Form.Item>
      <Form.Item
        name="password"
        label="Password"
        tooltip="Please check your password before submitting the form."
        rules={[
          {
            required: true,
            message: 'Password is required.',
          },
        ]}
      >
        <Input.Password prefix={<LockOutlined className="icon-opacity" />} />
      </Form.Item>
      <Form.Item
        name="confirmPassword"
        label="Confirm password"
        dependencies={['password']}
        hasFeedback
        rules={[
          {
            required: true,
            message: 'Please confirm your password.',
          },
          ({ getFieldValue }) => ({
            async validator(_, value: boolean) {
              if (!value || getFieldValue('password') === value) {
                await Promise.resolve();
                return;
              }
              return await Promise.reject(
                new Error('The new password that you entered do not match.')
              );
            },
          }),
        ]}
      >
        <Input.Password prefix={<LockOutlined className="icon-opacity" />} />
      </Form.Item>
      <Form.Item
        name="agreement"
        valuePropName="checked"
        rules={[
          {
            validator: async (_, value: boolean) => {
              value
                ? await Promise.resolve()
                : await Promise.reject(new Error('Should accept agreement.'));
            },
          },
        ]}
        {...tailFormItemLayout}
      >
        <Checkbox>
          I have read the <a href="">agreement</a>
        </Checkbox>
      </Form.Item>
      <Form.Item {...tailFormItemLayout}>
        <Button type="primary" htmlType="submit" className="">
          Register
        </Button>
      </Form.Item>
      <Form.Item {...tailFormItemLayout}>
        Already have an account? Log in <Link to="/login">here</Link>.
      </Form.Item>
    </Form>
  );
};

export default RegistrationForm;
