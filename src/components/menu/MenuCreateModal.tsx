import { Form, Input, InputNumber, Modal } from 'antd';
import React from 'react';
import { type DishThunk } from './MenuTable';
import TextArea from 'antd/es/input/TextArea';
import { type ApiDishesResponse, createDish } from '../../api/dishes';
import { useAppSelector } from '../../app/hooks';

interface MenuCreateModalProps {
  isModalOpen: boolean;
  handleCancel: () => void;
  setResponseMethod: (value: ApiDishesResponse) => void;
}

const MenuCreateModal = ({
  isModalOpen,
  handleCancel,
  setResponseMethod,
}: MenuCreateModalProps): JSX.Element => {
  const [form] = Form.useForm();

  const jwt = useAppSelector(state => {
    return state.authentication.jwt;
  });

  const onCreate = async (values: DishThunk): Promise<void> => {
    await createDish(values, jwt)
      .then(response => {
        console.log(response);
        setResponseMethod(response);
      })
      .catch(error => {
        console.log(error.response);
        setResponseMethod(error.response);
      });
  };

  return (
    <Modal
      title={`Let's create a new dish.`}
      open={isModalOpen}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            form.resetFields();
            void onCreate(values);
            handleCancel();
          })
          .catch(info => {
            console.log('Validate Failed:', info);
          });
      }}
      onCancel={handleCancel}
    >
      <Form
        form={form}
        layout="vertical"
        name="create_dish_form_in_modal"
        initialValues={{ modifier: 'public' }}
      >
        <Form.Item
          name="dishName"
          label="Name"
          rules={[
            {
              required: true,
              message: 'Please input the name for dish.',
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="description"
          label="Description"
          rules={[
            {
              required: true,
              message: 'Please input the description for dish.',
            },
          ]}
        >
          <TextArea rows={4} maxLength={255} />
        </Form.Item>
        <Form.Item
          name="price"
          label="Price"
          rules={[
            {
              required: true,
              message: 'Please input the price for dish.',
            },
          ]}
        >
          <InputNumber min={1} step={0.5} />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default MenuCreateModal;
