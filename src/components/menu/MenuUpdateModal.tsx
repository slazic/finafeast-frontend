import { Form, Input, InputNumber, Modal } from 'antd';
import React from 'react';
import { type Dish } from './MenuTable';
import TextArea from 'antd/es/input/TextArea';
import { type ApiDishesResponse, updateDish } from '../../api/dishes';
import { useAppSelector } from '../../app/hooks';

interface MenuUpdateModalProps {
  dish?: Dish;
  isModalOpen: boolean;
  setResponseMethod: (value: ApiDishesResponse) => void;
  handleCancel: () => void;
}

const MenuUpdateModal = ({
  dish,
  isModalOpen,
  setResponseMethod,
  handleCancel,
}: MenuUpdateModalProps): JSX.Element => {
  const [form] = Form.useForm();

  form.setFieldsValue({
    id: dish?.id,
    dishName: dish?.dishName,
    description: dish?.description,
    price: dish?.price,
  });

  const jwt = useAppSelector(state => {
    return state.authentication.jwt;
  });

  const onUpdate = async (values: Dish): Promise<void> => {
    await updateDish(values, jwt)
      .then(response => {
        console.log(response);
        setResponseMethod(response);
      })
      .catch(error => {
        console.log(error.response);
        setResponseMethod(error.response);
      });
  };

  return (
    <Modal
      title={'How do you wish to update the dish -> id: ' + dish?.id + '?'}
      open={isModalOpen}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            void onUpdate(values);
            form.resetFields();
            handleCancel();
          })
          .catch(info => {
            console.log('Validate Failed:', info);
          });
      }}
      onCancel={() => {
        form.resetFields();
        handleCancel();
      }}
    >
      <Form
        form={form}
        layout="vertical"
        name="create_dish_form_in_modal"
        initialValues={{
          id: '',
          dishName: '',
          description: '',
          price: '',
        }}
      >
        <Form.Item name="id" />
        <Form.Item
          name="dishName"
          label="Name"
          rules={[
            {
              required: true,
              message: 'Please input the name for dish.',
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="description"
          label="Description"
          rules={[
            {
              required: true,
              message: 'Please input the description for dish.',
            },
          ]}
        >
          <TextArea rows={4} maxLength={255} />
        </Form.Item>
        <Form.Item
          name="price"
          label="Price"
          rules={[
            {
              required: true,
              message: 'Please input the price for dish.',
            },
          ]}
        >
          <InputNumber min={1} step={0.5} />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default MenuUpdateModal;
