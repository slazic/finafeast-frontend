import { Modal } from 'antd';
import React from 'react';
import { type Dish } from './MenuTable';
import { type ApiDishesResponse, deleteDish } from '../../api/dishes';
import { useAppSelector } from '../../app/hooks';

interface MenuDeleteModalProps {
  dish?: Dish;
  isModalOpen: boolean;
  handleCancel: () => void;
  setResponseMethod: (value: ApiDishesResponse) => void;
}

const MenuDeleteModal = ({
  dish,
  isModalOpen,
  handleCancel,
  setResponseMethod,
}: MenuDeleteModalProps): JSX.Element => {
  const jwt = useAppSelector(state => {
    return state.authentication.jwt;
  });

  const handleDeleteDish = async (value: number | undefined): Promise<void> => {
    await deleteDish(value, jwt)
      .then(response => {
        console.log(response);
        setResponseMethod(response);
      })
      .catch(error => {
        console.log(error);
        setResponseMethod(error.response);
      });
  };

  return (
    <Modal
      title={'Do you wish to delete dish -> id: ' + dish?.id + '?'}
      open={isModalOpen}
      onOk={() => {
        void handleDeleteDish(dish?.id);
        handleCancel();
      }}
      onCancel={handleCancel}
    ></Modal>
  );
};

export default MenuDeleteModal;
