import React, { useEffect, useState } from 'react';
import { type ApiDishesResponse, fetchAllDishes } from '../../api/dishes';
import { Button, Space, Table } from 'antd';
import { type ColumnsType } from 'antd/es/table';
import {
  DeleteFilled,
  EditFilled,
  PlusCircleOutlined,
} from '@ant-design/icons';
import MenuDeleteModal from './MenuDeleteModal';
import MenuCreateModal from './MenuCreateModal';
import MenuUpdateModal from './MenuUpdateModal';
import { useAppSelector } from '../../app/hooks';

export interface Dish {
  id: number;
  dishName: string;
  description: string;
  price: number;
}

export interface DishThunk {
  dishName: string;
  description: string;
  price: number;
}

const MenuTable = (): JSX.Element => {
  const [dishes, setDishes] = useState<Dish[]>();
  const [loading, setLoading] = useState<boolean>(true);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState<boolean>(false);
  const [isCreateModalOpen, setIsCreateModalOpen] = useState<boolean>(false);
  const [isUpdateModalOpen, setIsUpdateModalOpen] = useState<boolean>(false);
  const [response, setResponse] = useState<ApiDishesResponse>();

  const [selectedDish, setSelectedDish] = useState<Dish>();

  const userRoles = useAppSelector(state => {
    return state.authentication.userRoles;
  });

  const showDeleteModal = (dishId: number): void => {
    const selectedDish = dishes?.filter(dish => dish.id === dishId)[0];
    setSelectedDish(selectedDish);
    setIsDeleteModalOpen(true);
  };

  const showUpdateModal = (dishId: number): void => {
    const selectedDish = dishes?.filter(dish => dish.id === dishId)[0];
    setSelectedDish(selectedDish);
    setIsUpdateModalOpen(true);
  };

  const showCreateModal = (): void => {
    setIsCreateModalOpen(true);
  };

  const columns: ColumnsType<Dish> = [
    {
      title: 'Name',
      dataIndex: 'dishName',
      key: 'name',
    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
    },
    userRoles.includes('ADMIN')
      ? {
          title: 'Action',
          key: 'action',
          render: (_, record) => (
            <Space size="middle">
              <Button
                type="link"
                onClick={() => {
                  showUpdateModal(record.id);
                }}
                title="Update"
              >
                <EditFilled className="icon-lg" />
              </Button>
              <Button
                type="link"
                onClick={() => {
                  showDeleteModal(record.id);
                }}
                title="Delete"
              >
                <DeleteFilled className="icon-lg" />
              </Button>
            </Space>
          ),
        }
      : {},
  ];

  useEffect(() => {
    const loadAllDishes = async (): Promise<void> => {
      await fetchAllDishes()
        .then(response => {
          setDishes(response.data);
        })
        .catch(error => {
          console.log(error.response);
          if (
            error.response === undefined ||
            (error.response.data === 'Dishes not found.' &&
              error.response.status === 404)
          ) {
            setDishes([]);
          }
        })
        .finally(() => {
          setLoading(false);
        });
    };

    void loadAllDishes();
  }, [response]);

  return (
    <>
      {userRoles.includes('ADMIN') || userRoles.includes('MANAGER') ? (
        <Button
          icon={<PlusCircleOutlined />}
          onClick={() => {
            showCreateModal();
          }}
          title="Create new dish"
        >
          Create new dish
        </Button>
      ) : (
        <></>
      )}
      <Table
        columns={columns}
        dataSource={dishes}
        loading={loading}
        className="mt-6 mx"
      />
      <MenuDeleteModal
        dish={selectedDish}
        isModalOpen={isDeleteModalOpen}
        setResponseMethod={setResponse}
        handleCancel={() => {
          setIsDeleteModalOpen(false);
        }}
      />
      <MenuCreateModal
        isModalOpen={isCreateModalOpen}
        setResponseMethod={setResponse}
        handleCancel={() => {
          setIsCreateModalOpen(false);
        }}
      />
      <MenuUpdateModal
        dish={selectedDish}
        isModalOpen={isUpdateModalOpen}
        setResponseMethod={setResponse}
        handleCancel={() => {
          setIsUpdateModalOpen(false);
        }}
      />
    </>
  );
};

export default MenuTable;
