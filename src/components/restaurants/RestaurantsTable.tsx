import { Button, Space, Table, type TableColumnsType } from 'antd';
import React, { useEffect, useState } from 'react';
import {
  type ApiRestaurantsResponse,
  fetchAllRestaurants,
} from '../../api/restaurants';
import {
  DeleteFilled,
  EditFilled,
  PlusCircleOutlined,
} from '@ant-design/icons';
import { useAppSelector } from '../../app/hooks';
import RestaurantsCreateModal from './RestaurantsCreateModal';
import RestaurantsDeleteModal from './RestaurantsDeleteModal';
import RestaurantsUpdateModal from './RestaurantsUpdateModal';

export interface Manager {
  key: number;
  id: number;
  firstName: string;
  lastName: string;
  phone: string;
  email: string;
}

export interface Restaurant {
  key: number;
  id: number;
  name: string;
  phone: string;
  address: string;
  manager: Manager;
}

export interface RestaurantThunk {
  name: string;
  phone: string;
  address: string;
  managerId: number;
}

const RestaurantsTable = (): JSX.Element => {
  const [restaurants, setRestaurants] = useState<Restaurant[]>();
  const [selectedRestaurant, setSelectedRestaurant] = useState<Restaurant>();
  const [loading, setLoading] = useState<boolean>(true);
  const [isCreateModalOpen, setIsCreateModalOpen] = useState<boolean>(false);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState<boolean>(false);
  const [isUpdateModalOpen, setIsUpdateModalOpen] = useState<boolean>(false);

  const [response, setResponse] = useState<ApiRestaurantsResponse>();

  const userRoles = useAppSelector(state => {
    return state.authentication.userRoles;
  });

  const showCreateModal = (): void => {
    setIsCreateModalOpen(true);
  };

  const showDeleteModal = (selectedRestaurantId: number): void => {
    const selectedRestaurant = restaurants?.filter(
      restaurant => restaurant.id === selectedRestaurantId
    )[0];

    setSelectedRestaurant(selectedRestaurant);
    setIsDeleteModalOpen(true);
  };

  const showUpdateModal = (selectedRestaurantId: number): void => {
    const selectedRestaurant = restaurants?.filter(
      restaurant => restaurant.id === selectedRestaurantId
    )[0];

    setSelectedRestaurant(selectedRestaurant);
    setIsUpdateModalOpen(true);
  };

  useEffect(() => {
    const loadAllRestaurants = async (): Promise<void> => {
      await fetchAllRestaurants()
        .then(response => {
          setRestaurants(response.data);
        })
        .catch(error => {
          console.log(error.response);
          if (
            error.response === undefined ||
            (error.response.status === 404 &&
              error.response.data === 'Restaurants not found.')
          ) {
            setRestaurants([]);
          }
        })
        .finally(() => {
          setLoading(false);
        });
    };

    void loadAllRestaurants();
  }, [response]);

  const expandedRowRender = (record: Restaurant): JSX.Element => {
    const columns: TableColumnsType<Manager> = [
      {
        title: 'Manager',
        colSpan: 2,
        dataIndex: 'firstName',
        key: 'firstName',
      },
      // { title: 'First name', dataIndex: 'firstName', key: 'firstName' },
      {
        title: 'Last name',
        colSpan: 0,
        dataIndex: 'lastName',
        key: 'lastName',
      },
      { title: 'Phone', dataIndex: 'phone', key: 'phone' },
      { title: 'Email', dataIndex: 'email', key: 'email' },
    ];

    const managers: Manager[] | undefined = restaurants
      ?.filter(restaurant => restaurant.id === record.id)
      .map(restaurant => restaurant.manager);
    return <Table columns={columns} dataSource={managers} pagination={false} />;
  };

  const columns: TableColumnsType<Restaurant> = [
    { title: 'Name', dataIndex: 'name', key: 'name' },
    { title: 'Phone', dataIndex: 'phone', key: 'phone' },
    { title: 'Address', dataIndex: 'address', key: 'address' },
    userRoles.includes('ADMIN')
      ? {
          title: 'Action',
          key: 'action',
          render: (_, record) => (
            <Space size="middle">
              <Button
                type="link"
                onClick={() => {
                  showUpdateModal(record.id);
                }}
                title="Update"
              >
                <EditFilled className="icon-lg" />
              </Button>
              <Button
                type="link"
                onClick={() => {
                  showDeleteModal(record.id);
                }}
                title="Delete"
              >
                <DeleteFilled className="icon-lg" />
              </Button>
            </Space>
          ),
        }
      : {},
  ];

  const data: Restaurant[] | undefined = restaurants?.map(restaurant => {
    return { ...restaurant, key: restaurant.id };
  });

  return (
    <>
      {userRoles.includes('ADMIN') ? (
        <Button
          icon={<PlusCircleOutlined />}
          onClick={() => {
            showCreateModal();
          }}
          title="Upload new restaurant"
        >
          Upload new restaurant
        </Button>
      ) : (
        <></>
      )}
      <Table
        columns={columns}
        expandable={{
          expandedRowRender,
          defaultExpandedRowKeys: ['0'],
        }}
        dataSource={data}
        loading={loading}
        className="mt-6 mx"
      />
      <RestaurantsCreateModal
        isModalOpen={isCreateModalOpen}
        handleCancel={() => {
          setIsCreateModalOpen(false);
        }}
        setResponseMethod={setResponse}
      />
      <RestaurantsDeleteModal
        restaurant={selectedRestaurant}
        isModalOpen={isDeleteModalOpen}
        handleCancel={() => {
          setIsDeleteModalOpen(false);
        }}
        setResponseMethod={setResponse}
      />
      <RestaurantsUpdateModal
        restaurant={selectedRestaurant}
        isModalOpen={isUpdateModalOpen}
        handleCancel={() => {
          setIsUpdateModalOpen(false);
        }}
        setResponseMethod={setResponse}
      />
    </>
  );
};

export default RestaurantsTable;
