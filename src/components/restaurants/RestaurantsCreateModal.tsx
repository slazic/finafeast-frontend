import { Form, Input, Modal, Select } from 'antd';
import { useForm } from 'antd/es/form/Form';
import React, { useEffect, useState } from 'react';
import { type Manager, type RestaurantThunk } from './RestaurantsTable';
import { fetchAllUsersByRole } from '../../api/users';
import { useAppSelector } from '../../app/hooks';
import {
  type ApiRestaurantsResponse,
  createRestaurant,
} from '../../api/restaurants';

interface RestaurantsCreateModalProps {
  isModalOpen: boolean;
  handleCancel: () => void;
  setResponseMethod: (response: ApiRestaurantsResponse) => void;
}

interface RestourantCreateFormProps {
  name: string;
  phonePrefix: string;
  phone: string;
  address: string;
  managerId: number;
}

const RestaurantsCreateModal = ({
  isModalOpen,
  handleCancel,
  setResponseMethod,
}: RestaurantsCreateModalProps): JSX.Element => {
  const [form] = useForm();
  const [loading, setLoading] = useState<boolean>(true);
  const [managers, setManagers] = useState<Manager[]>();
  const jwt = useAppSelector(state => {
    return state.authentication.jwt;
  });

  useEffect(() => {
    const loadAllManagers = async (): Promise<void> => {
      await fetchAllUsersByRole('MANAGER', jwt)
        .then(response => {
          setManagers(response.data);
        })
        .catch(error => {
          console.log(error);
          if (
            error.response === undefined ||
            (error.response.status === 404 &&
              error.response.data === 'Users not found.')
          ) {
            setManagers([]);
          }
        })
        .finally(() => {
          setLoading(false);
        });
    };

    void loadAllManagers();
  }, []);

  const onCreate = async (values: RestourantCreateFormProps): Promise<void> => {
    const restaurantThunk: RestaurantThunk = {
      name: values.name,
      phone: values.phonePrefix + values.phone,
      address: values.address,
      // values.address[1] + ' ' + values.address[2] + ', ' + values.address[0],
      managerId: values.managerId,
    };
    await createRestaurant(restaurantThunk, jwt)
      .then(response => {
        setResponseMethod(response);
      })
      .catch(error => {
        setResponseMethod(error.response);
      });
  };

  const prefixSelector = (
    <Form.Item name="phonePrefix" noStyle>
      <Select>
        <Select.Option value={'+385'}>+385</Select.Option>
      </Select>
    </Form.Item>
  );

  return (
    <Modal
      title={`Let's upload a new restaurant.`}
      open={isModalOpen}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            form.resetFields();
            void onCreate(values);
            handleCancel();
          })
          .catch(info => {
            console.log('Validate Failed:', info);
          });
      }}
      onCancel={handleCancel}
    >
      <Form
        form={form}
        layout="vertical"
        name="create_dish_form_in_modal"
        initialValues={{ phonePrefix: '+385', modifier: 'public' }}
      >
        <Form.Item
          name="name"
          label="Name"
          rules={[
            {
              required: true,
              message: 'Please input the name for restaurant.',
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="phone"
          label="Phone"
          rules={[
            {
              required: true,
              message: 'Please input the phone number for restaurant.',
            },
          ]}
        >
          <Input addonBefore={prefixSelector} style={{ width: '100%' }} />
        </Form.Item>
        <Form.Item
          name="address"
          label="Address"
          rules={[
            {
              required: true,
              message: 'Please input the address for restaurant.',
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="managerId"
          label="Manager"
          rules={[
            {
              required: true,
              message: 'Please select the manager for restaurant.',
            },
          ]}
        >
          <Select
            options={managers?.map(manager => {
              return {
                value: manager.id,
                label:
                  manager.firstName +
                  ' ' +
                  manager.lastName +
                  ' / ' +
                  manager.phone +
                  ' / ' +
                  manager.email,
              };
            })}
            loading={loading}
            allowClear
          />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default RestaurantsCreateModal;
