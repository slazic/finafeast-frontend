import React, { useEffect, useState } from 'react';
import { type Manager, type Restaurant } from './RestaurantsTable';
import { Form, Input, Modal, Select } from 'antd';
import { useForm } from 'antd/es/form/Form';
import { useAppSelector } from '../../app/hooks';
import { fetchAllUsersByRole } from '../../api/users';
import {
  updateRestaurant,
  type ApiUpdateRestaurantRequest,
  type ApiRestaurantsResponse,
} from '../../api/restaurants';

interface RestaurantsUpdateModalProps {
  restaurant?: Restaurant;
  isModalOpen: boolean;
  handleCancel: () => void;
  setResponseMethod: (response: ApiRestaurantsResponse) => void;
}

interface RestourantUpdateFormProps {
  id: number;
  name: string;
  phonePrefix: string;
  phone: string;
  address: string;
  managerId: number;
}

const RestaurantsUpdateModal = ({
  restaurant,
  isModalOpen,
  handleCancel,
  setResponseMethod,
}: RestaurantsUpdateModalProps): JSX.Element => {
  const [form] = useForm();

  form.setFieldsValue({
    id: restaurant?.id,
    name: restaurant?.name,
    phonePrefix: '+385',
    phone: restaurant?.phone.slice(4),
    address: restaurant?.address,
    managerId: restaurant?.manager.id,
  });

  const [loading, setLoading] = useState<boolean>(true);
  const [managers, setManagers] = useState<Manager[]>();
  const jwt = useAppSelector(state => {
    return state.authentication.jwt;
  });

  const onUpdate = async (values: RestourantUpdateFormProps): Promise<void> => {
    const updatedRestaurant: ApiUpdateRestaurantRequest = {
      id: values.id,
      name: values.name,
      phone: values.phonePrefix + values.phone,
      address: values.address,
      managerId: values.managerId,
    };
    await updateRestaurant(updatedRestaurant, jwt)
      .then(response => {
        setResponseMethod(response);
      })
      .catch(error => {
        setResponseMethod(error.response);
      });
  };

  useEffect(() => {
    const loadAllManagers = async (): Promise<void> => {
      await fetchAllUsersByRole('MANAGER', jwt)
        .then(response => {
          setManagers(response.data);
        })
        .catch(error => {
          console.log(error);
          if (
            error.response === undefined ||
            (error.response.status === 404 &&
              error.response.data === 'Users not found.')
          ) {
            setManagers([]);
          }
        })
        .finally(() => {
          setLoading(false);
        });
    };

    void loadAllManagers();
  }, []);

  const prefixSelector = (
    <Form.Item name="phonePrefix" noStyle>
      <Select>
        <Select.Option value={'+385'}>+385</Select.Option>
      </Select>
    </Form.Item>
  );

  return (
    <Modal
      title={
        'How do you wish to update the restaurant -> id: ' +
        restaurant?.id +
        '?'
      }
      open={isModalOpen}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            void onUpdate(values);
            form.resetFields();
            handleCancel();
          })
          .catch(info => {
            console.log('Validate Failed:', info);
          });
      }}
      onCancel={() => {
        form.resetFields();
        handleCancel();
      }}
    >
      <Form
        form={form}
        layout="vertical"
        name="create_dish_form_in_modal"
        initialValues={{
          id: '',
          name: '',
          phonePrefix: '+385',
          phone: '',
          address: '',
          managerId: '',
        }}
      >
        <Form.Item name="id" />
        <Form.Item
          name="name"
          label="Name"
          rules={[
            {
              required: true,
              message: 'Please input the name for restaurant.',
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="phone"
          label="Phone"
          rules={[
            {
              required: true,
              message: 'Please input the phone number for restaurant.',
            },
          ]}
        >
          <Input addonBefore={prefixSelector} style={{ width: '100%' }} />
        </Form.Item>
        <Form.Item
          name="address"
          label="Address"
          rules={[
            {
              required: true,
              message: 'Please input the address for restaurant.',
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="managerId"
          label="Manager"
          rules={[
            {
              required: true,
              message: 'Please select the manager for restaurant.',
            },
          ]}
        >
          <Select
            options={managers?.map(manager => {
              return {
                value: manager.id,
                label:
                  manager.firstName +
                  ' ' +
                  manager.lastName +
                  ' / ' +
                  manager.phone +
                  ' / ' +
                  manager.email,
              };
            })}
            loading={loading}
            allowClear
          />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default RestaurantsUpdateModal;
