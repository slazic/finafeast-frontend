import React from 'react';
import { type Restaurant } from './RestaurantsTable';
import { Modal } from 'antd';
import {
  deleteRestaurant,
  type ApiRestaurantsResponse,
} from '../../api/restaurants';
import { useAppSelector } from '../../app/hooks';

interface RestaurantsDeleteModalProps {
  restaurant: Restaurant | undefined;
  isModalOpen: boolean;
  handleCancel: () => void;
  setResponseMethod: (response: ApiRestaurantsResponse) => void;
}

const RestaurantsDeleteModal = ({
  restaurant,
  isModalOpen,
  handleCancel,
  setResponseMethod,
}: RestaurantsDeleteModalProps): JSX.Element => {
  const jwt = useAppSelector(state => {
    return state.authentication.jwt;
  });

  const handleDeleteRestaurant = async (
    value: number | undefined
  ): Promise<void> => {
    await deleteRestaurant(value, jwt)
      .then(response => {
        setResponseMethod(response);
      })
      .catch(error => {
        setResponseMethod(error.response);
      });
  };

  return (
    <Modal
      title={'Do you wish to delete restaurant -> id: ' + restaurant?.id + '?'}
      open={isModalOpen}
      onOk={() => {
        void handleDeleteRestaurant(restaurant?.id);
        handleCancel();
      }}
      onCancel={handleCancel}
    ></Modal>
  );
};

export default RestaurantsDeleteModal;
